import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CupertinoActivityIndicator(
        radius: 40,
      ),
    );
  }
}
