import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Function function;

  const CustomButton({
    Key key,
    this.function,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
          side: BorderSide(color: Colors.red)),
      onPressed: function,
      color: Colors.red,
      textColor: Colors.white,
      child: Text("Reset".toUpperCase(), style: TextStyle(fontSize: 14)),
    );
  }
}
