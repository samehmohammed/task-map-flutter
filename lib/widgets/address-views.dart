import 'package:flutter/material.dart';

class AdressView extends StatefulWidget {
  final List<String> addresses;

  const AdressView({Key key, this.addresses}) : super(key: key);
  @override
  _AdressViewState createState() => _AdressViewState();
}

class _AdressViewState extends State<AdressView> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: widget.addresses.length == 0
            ? Center(
                child: Container(
                  margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 2),
                      borderRadius: BorderRadius.circular(8)),
                  child: Text("Select"),
                ),
              )
            : Column(
                children: widget.addresses.map((e) {
                  return Container(
                    margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2),
                        borderRadius: BorderRadius.circular(8)),
                    child: Text(e),
                  );
                }).toList(),
              ));
  }
}
