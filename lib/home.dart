import 'package:bot_toast/bot_toast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:task/view-model/add-road-view-model.dart';
import 'package:task/view-model/response-status.dart';
import 'package:task/widgets/address-views.dart';
import 'package:task/widgets/custom-button.dart';
import 'package:task/widgets/custom-indicator.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  Set<Marker> _markers = new Set<Marker>();
  int _id = 1;
  Set<Polygon> _polygonSet = new Set();
  List<LatLng> polygonCoords = new List();
  List<String> addresses = [];
  AddRoadToFireStoreModelView services = new AddRoadToFireStoreModelView();
  addMarkerAndDrawRoad(LatLng place) {
    setState(() {
      _id++;
      String markId = (_id + 1).toString();
      if (polygonCoords.length < 2) {
        _markers.add(Marker(
          markerId: MarkerId(markId.toString()),
          position: LatLng(place.latitude, place.longitude),
        ));
        polygonCoords.add(LatLng(place.latitude, place.longitude));

        _getLocation(place, polygonCoords.length);
      } else {
        BotToast.showSimpleNotification(
            title: "Attention",
            subTitle: "Press Reset To redraw",
            duration: Duration(seconds: 4),
            backgroundColor: Colors.grey[50]);
      }
      _polygonSet.add(Polygon(
          polygonId: PolygonId('drawRoad'),
          points: polygonCoords,
          strokeWidth: 8,
          strokeColor: Colors.red));
    });
  }

  _getLocation(LatLng position, int polygonCoordsLength) async {
    final coordinates = new Coordinates(position.latitude, position.longitude);

    await Geocoder.local
        .findAddressesFromCoordinates(coordinates)
        .then((value) {
      var first = value.first;
      setState(() {
        !addresses.contains(first.addressLine)
            ? addresses.add(first.addressLine)
            : null;
        if (polygonCoords.length == 2 && addresses.length == 2) {
          double distanceInMeters = Geolocator.distanceBetween(
              polygonCoords[0].latitude,
              polygonCoords[0].longitude,
              polygonCoords[1].latitude,
              polygonCoords[1].longitude);

          services.postData(
              data: {
                'LocationFrom': GeoPoint(
                    polygonCoords[0].latitude, polygonCoords[0].longitude),
                'LocationTo': GeoPoint(
                    polygonCoords[1].latitude, polygonCoords[1].longitude),
                'distance': distanceInMeters,
                'placeFrom': addresses[0],
                'placeTo': addresses[1],
              },
              onComplete: () {
                BotToast.showSimpleNotification(
                    title: "Success",
                    subTitle: "Item Saved To FireStore",
                    duration: Duration(seconds: 4),
                    backgroundColor: Colors.grey[50]);
              });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Task"),
      ),
      body: Container(
        child: Stack(
          children: [
            ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: [
                AdressView(
                  addresses: addresses,
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 450,
                  child: GoogleMap(
                    onTap: addMarkerAndDrawRoad,
                    polygons: _polygonSet,
                    mapType: MapType.normal,
                    initialCameraPosition: CameraPosition(
                        target: LatLng(30.033333, 31.233334),
                        zoom: 14.5), // cairo
                    onMapCreated: (GoogleMapController controller) {},
                    markers: _markers,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                    height: 40,
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: CustomButton(
                      function: () {
                        setState(() {
                          polygonCoords = [];
                          _markers = new Set<Marker>();
                          _polygonSet = Set<Polygon>();
                          addresses = [];
                        });
                      },
                    )),
                SizedBox(
                  height: 20,
                ),
              ],
            ),

            // listen to response change and display loader depend on response statue value
            ChangeNotifierProvider<AddRoadToFireStoreModelView>(
              create: (BuildContext context) => services,
              child: Consumer<AddRoadToFireStoreModelView>(
                builder: (context, status, child) {
                  switch (status.respose['status']) {
                    case ResponseState.LOADING:
                      return Center(child: CustomIndicator());
                  }
                  return Container();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
