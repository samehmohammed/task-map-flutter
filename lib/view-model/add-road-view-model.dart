import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:task/view-model/response-status.dart';

class AddRoadToFireStoreModelView extends ChangeNotifier {
  Map<String, dynamic> _response = {'status': ResponseState.SLEEP};
  Map<String, dynamic> get respose => _response;
  postData({Map<String, dynamic> data, Function onComplete}) {
    _response = {'status': ResponseState.LOADING};
    notifyListeners();
    FirebaseFirestore.instance.collection("directions").add(data).then((value) {
      // time to show provider for 3 second then didapear
      Future.delayed(Duration(seconds: 3), () {
        _response = {'status': ResponseState.COMPLETE};
        notifyListeners();
      });

      onComplete();
    });
  }
}
